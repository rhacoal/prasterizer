#ifndef PRASTERIZER_RASTERIZER_H
#define PRASTERIZER_RASTERIZER_H
#include "common.h"
#include "model.h"

#include <cmath>
#include <cstdint>
#include <memory>

struct framebuffer {
  texture color;
  std::unique_ptr<float[]> depth;

  framebuffer(uint32_t width, uint32_t height);

  void save_to_rgba_uint8(uint8_t *data) const;

  uint32_t width() const;

  uint32_t height() const;
};

class context {
public:
  context(uint32_t width, uint32_t height);

  void resize(uint32_t width, uint32_t height);

  uint32_t width() const;

  uint32_t height() const;

  framebuffer &frambuffer();

  void clear_color(float r, float g, float b, float a, float depth);

  void draw_facets(shader *program, const facet_geometry *geometry);

  void save_framebuffer(uint8_t *data);

private:
  framebuffer fb;
};
#endif

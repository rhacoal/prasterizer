#include "programs.h"
#include "bxdf.h"

thread_local std::mt19937 engine = []() {
  std::random_device rd;
  return std::mt19937{rd()};
}();

void ray_closest_hit(traverse_context *ctx, void *payload) {
  auto *hit = get_intersection(ctx);
  auto *obj = reinterpret_cast<mesh *>(payload);
  auto *params = reinterpret_cast<launch *>(get_launch_params(ctx));
  std::uniform_real_distribution<float> distrib{};

  const auto interpolate = [&](float3 bary, auto *f) {
    return bary[0] * f[0] + bary[1] * f[1] + bary[2] * f[2];
  };
  uint32_t primitive_index = hit->primitive_index;
  float2 uv = interpolate(hit->barycentric,
                          obj->geometry.texcoord.data() + primitive_index * 3);
  float3 normal = interpolate(hit->barycentric, obj->geometry.normal.data() +
                                                    primitive_index * 3);
  float3 position = interpolate(
      hit->barycentric, obj->geometry.position.data() + primitive_index * 3);
  normal = normalize(mul(get_normal_transform(ctx), normal));
  position = mul(get_world_transform(ctx), float4(position, 1.0f)).xyz();
  if (hit->side) {
    normal = -normal;
  }

  float4 albedo = shader::sample(obj->diffuse, uv);
  float3 specular = lerp(float3(0.04f), albedo.xyz(), obj->metallic);
  float3 diffuse = albedo.xyz() * (1.0f - obj->metallic);
  ray_data *prd = reinterpret_cast<ray_data *>(get_per_ray_ptr(ctx));
  float rr_p = 0.0f;
  if (prd->depth > 8) {
    rr_p = std::max(1.0f, linalg::maxelem(prd->color));
    if (distrib(engine) < rr_p) {
      // stops ray raytracing
      prd->pdf *= rr_p;
      return;
    } else {
      prd->pdf *= 1 - rr_p;
    }
  }
  prd->depth += 1;
  float3 dir = get_ray_direction(ctx);
  float p = distrib(engine);
  float p_reflect = obj->opacity;
  float p_transmission = 1.0f - obj->opacity;
  if (p < p_reflect) {
    prd->pdf *= p_reflect;
    float3 u = normalize(cross((fabs(normal.x) > .1 ? float3(0.0f, 1.0f, 0.0f)
                                                    : float3(1.0f, 0.0f, 0.0f)),
                               normal));
    float3 v = cross(normal, u);
    // rotate directions so that normal is the y-axis
    float p_specular = obj->metallic * p_reflect;
    if (p < p_specular) {
      // * specular
      prd->pdf *= p_specular;
      float3x3 normal2world{u, normal, v};
      float3 wo = normalize(mul(transpose(normal2world), dir));
      // VNDF sampling
      float r0 = distrib(engine), r1 = distrib(engine);
      float3 wi, reflectance;
      ImportanceSampleGgxVdn(r0, r1, -wo, obj->roughness, specular, wi,
                             reflectance);
      prd->color = float3{};
      if (BsdfNDot(wi) > 0.0f) {
        float3 reflect_dir = normalize(mul(normal2world, wi));
        launch_ray(ctx, params->handle, position, reflect_dir, 1e-4f, 1e20f);
      }
      prd->color = obj->emission + prd->color * reflectance;
    } else {
      prd->pdf *= (1 - p_specular);
      float r1 = distrib(engine) * 2.0f * math::pi<float>(),
            r2 = distrib(engine);
      float r2s = std::sqrt(r2);
      float cosine = std::sqrt(1 - r2);
      float3 diffuse_dir = normalize(
          (u * std::cos(r1) * r2s + v * std::sin(r1) * r2s + normal * cosine));
      launch_ray(ctx, params->handle, position, diffuse_dir, 1e-4f, 1e20f);
      // lambertian = diffuse * (1 / pi) * (1 / (cosine / pi)) * cosine = diffuse
      //              ^   reflectance  ^        ^    pdf    ^    ^ cos ^
      float3 lambertian = diffuse;
      prd->color = obj->emission + prd->color * lambertian;
    }
  } else {
    // * transmission
    float eta = hit->side ? obj->ior : 1.0f / obj->ior;
    float s = dot(normal, dir);
    float k = 1.0f - eta * eta * (1.0f - s * s);
    float3 next_dir;
    prd->pdf *= p_transmission;
    if (k < 0.0f) {
      // total reflection
      next_dir = dir - dot(dir, normal) * 2.0f * normal;
    } else {
      // refraction
      next_dir = eta * dir - (eta * s + std::sqrt(k)) * normal;
    }
    launch_ray(ctx, params->handle, position, next_dir, 1e-4f, 1e20f);
    prd->color = obj->emission + prd->color * albedo.xyz();
  }
  prd->color *= (1 - rr_p);
}

void ray_miss(traverse_context *ctx, void *payload) {
  float3 dir = get_ray_direction(ctx);
  dir = normalize(dir);
  float u = 0.5f + std::atan2(dir.x, -dir.z) / (2.0f * math::pi<float>());
  float v = 0.5f + std::asin(dir.y) / math::pi<float>();
  auto *params = reinterpret_cast<launch *>(get_launch_params(ctx));
  float3 env = shader::sample(*params->envmap, {u, v}).xyz() * params->envmap_intensity;
  reinterpret_cast<ray_data *>(get_per_ray_ptr(ctx))->color = env;
}

void raygen(traverse_context *ctx, void *launch_params, uint2 index) {
  auto &params = *reinterpret_cast<launch *>(launch_params);
  std::uniform_real_distribution<float> distrib{};
  float3 origin = params.camera_pos;

  uint32_t pixel_id = index.y * params.width + index.x;
  ray_data *prd = params.prd.data() + pixel_id;
  set_per_ray_ptr(ctx, prd);
  float3 integral = prd->color;
  float3 mean{};
  // accumulate one sample
  float ndc_x = ((index.x + distrib(engine)) * params.inv_w2) - 1.0f;
  float ndc_y = ((index.y + distrib(engine)) * params.inv_h2) - 1.0f;
  float3 near_pos{ndc_x * params.top * params.aspect, ndc_y * params.top,
                  -params.near};
  float3 world_pos = near_pos.x * params.camera_x +
                     near_pos.y * params.camera_y +
                     near_pos.z * (-params.camera_dir) + params.camera_pos;
  float3 direction = normalize(world_pos - params.camera_pos);

  *prd = ray_data{index.x, index.y, pixel_id, 1.0f, {}};
  launch_ray(ctx, params.handle, origin, direction, 0.0f, 1e20f);
  integral += clamp(prd->color / prd->pdf, float3(0.0f), float3(10.0f));
  mean = integral / (params.samples + 1);
  // stores accumulation in prd->color
  prd->color = integral;
  params.fb->color.data[pixel_id] = pow(float4(mean, 1.0f), 1.0f / 2.2f);
}
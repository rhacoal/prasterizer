#ifndef PRASTERIZER_RT_STRUCTURE_H
#define PRASTERIZER_RT_STRUCTURE_H
#include "common.h"

struct bounds3 {
  float3 p_min, p_max;

  bounds3()
      : p_min{std::numeric_limits<float>::max()},
        p_max{std::numeric_limits<float>::min()} {}

  explicit bounds3(float3 point) : p_min(point), p_max(point) {}

  bounds3(float3 p_min, float3 p_max) : p_min(p_min), p_max(p_max) {}

  bounds3 operator+(const bounds3 &rhs) const {
    return bounds3{min(p_min, rhs.p_min), max(p_max, rhs.p_max)};
  }

  bounds3 &operator+=(const bounds3 &rhs) {
    p_min = min(p_min, rhs.p_min);
    p_max = max(p_max, rhs.p_max);
    return *this;
  }

  bounds3 operator+(const float3 &rhs) const {
    return bounds3{min(p_min, rhs), max(p_max, rhs)};
  }

  bounds3 &operator+=(const float3 &rhs) {
    p_min = min(p_min, rhs);
    p_max = max(p_max, rhs);
    return *this;
  }

  uint8_t maxExtent() const {
    auto t = p_max - p_min;
    return t.x > t.y ? (t.x > t.z ? 0 : 2) : (t.y > t.z ? 1 : 2);
  }

  float3 centroid() const { return (p_max + p_min) * 0.5f; }
};

struct bvh_node {
  bounds3 bounds;
  uint32_t offset;
  uint16_t is_leaf;
  uint16_t dim;
  float padding[2];
};
#endif
# prasterizer - Preliminary Sofware Rasterizer

## Dependencies

- [glad](https://github.com/Dav1dde/glad)
- [glfw](https://github.com/glfw/glfw)
- [ImGui](https://github.com/ocornut/imgui)
- [stb_image](https://github.com/nothings/stb)
- [BS::thread_pool](https://github.com/bshoshany/thread-pool)
- [linalg.h](https://github.com/sgorsten/linalg)

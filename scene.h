#ifndef PRASTERIZER_SCENE_H
#define PRASTERIZER_SCENE_H
#include "common.h"
#include "model.h"

#include <filesystem>
#include <memory>

struct object {
  void set_position(const float3 &postion);
  void set_quaternion(const float4 &quat);
  void set_scale(const float3 &scale);
  const float3 &position() const;
  const float4 &quaternion() const;
  const float3 &scale() const;
  const float4x4 &matrix() const;
  virtual bool is_mesh() const;

private:
  mutable float4x4 _local{linalg::identity};
  void update_local_matrix() const;
  float3 _position{0.0f};
  float4 _quaternion{0.0f, 0.0f, 0.0f, 1.0f};
  float3 _scale{1.0f};
};

struct mesh : public object {
  mesh(facet_geometry geometry, texture diffuse);
  facet_geometry geometry;
  texture diffuse;
  virtual bool is_mesh() const override { return true; }

  float opacity{1.0f};
  float ior{1.0f};
  float metallic{0.0f};
  float roughness{1.0f};
  float3 emission{0.0f};
};
#endif

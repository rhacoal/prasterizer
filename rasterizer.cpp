#include "rasterizer.h"
#include "shader.h"

static float3 barycentric(float2 points[3], float x, float y) {
  float3 u = cross(float3{points[2].x - points[0].x, points[1].x - points[0].x,
                          points[0].x - x},
                   float3{points[2].y - points[0].y, points[1].y - points[0].y,
                          points[0].y - y});

  if (std::abs(u.z) < 1e-3) {
    return float3{-1, 1, 1};
  }
  return float3{1.f - (u.x + u.y) / u.z, u.y / u.z, u.x / u.z};
}

framebuffer::framebuffer(uint32_t width, uint32_t height)
    : color{width, height, std::make_unique<float4[]>(width * height)},
      depth{std::make_unique<float[]>(width * height)} {}

void framebuffer::save_to_rgba_uint8(uint8_t *data) const {
  for (uint32_t y = 0; y < color.height; ++y) {
    for (uint32_t x = 0; x < color.width; ++x) {
      uint32_t pixel_idx = (color.height - y - 1) * color.width + x;
      float4 pixel = color.data[y * color.width + x];
      for (int i = 0; i < 3; ++i) {
        data[pixel_idx * 4 + i] =
            std::clamp<int32_t>(std::lround(pixel[i] * 255.0f), 0, 255);
      }
      data[pixel_idx * 4 + 3] = 255;
    }
  }
}

uint32_t framebuffer::width() const { return color.width; }

uint32_t framebuffer::height() const { return color.height; }

context::context(uint32_t width, uint32_t height) : fb(width, height) {}

void context::resize(uint32_t width, uint32_t height) {
  fb = framebuffer(width, height);
}

uint32_t context::width() const { return fb.color.width; }

uint32_t context::height() const { return fb.color.height; }

framebuffer &context::frambuffer() { return fb; }

static float4x4 viewport_matrix(int x, int y, int w, int h) {
  return float4x4{
      {w / 2.0f, 0.0f, 0.0f, 0.0f},
      {0.0f, h / 2.0f, 0.0f, 0.0f},
      {0.0f, 0.0f, 0.5f, 0.0f},
      {w / 2.0f + x, h / 2.0f + y, 0.5f, 1.0f},
  };
}

void context::clear_color(float r, float g, float b, float a, float depth) {
  float4 color{r, g, b, a};
  std::fill_n(fb.color.data.get(), fb.color.width * fb.color.height, color);
  std::fill_n(fb.depth.get(), fb.color.width * fb.color.height, depth);
}

void context::draw_facets(shader *program, const facet_geometry *geometry) {
  size_t num_vert = geometry->normal.size();
  vertex_attributes attrib;
  program->use();
  float4 vertex_pos[3];
  float4 screen_perspective_pos[3];
  float2 screen_pos[3];
  auto viewport = viewport_matrix(0, 0, fb.color.width, fb.color.height);
  for (size_t i = 0; i < num_vert; i += 3) {
    // vertex
    for (uint32_t v = 0; v < 3; ++v) {
      attrib.normal = geometry->normal[i + v];
      attrib.position = geometry->position[i + v];
      attrib.texcoord = geometry->texcoord[i + v];
      program->set_vertex_index(v);
      auto glpos = program->vertex(attrib);
      vertex_pos[v] = glpos;
      screen_perspective_pos[v] = mul(viewport, glpos);
      screen_pos[v] = (screen_perspective_pos[v] / glpos.w).xy();
    }
    // behind the camera clipping
    if (vertex_pos[0].w <= 0 && vertex_pos[1].w <= 0 && vertex_pos[2].w <= 0) {
      continue;
    }
    // back-face culling
    float3 normal = cross(vertex_pos[1].xyz() - vertex_pos[0].xyz(),
                          vertex_pos[2].xyz() - vertex_pos[0].xyz());
    if (normal.z < 0.0f) {
      continue;
    }
    // rasterize
    int2 bbox_min{static_cast<int>(fb.color.width - 1),
                  static_cast<int>(fb.color.height - 1)};
    int2 bbox_max{0, 0};
    for (int i = 0; i < 3; i++) {
      bbox_min = min(bbox_min, int2(screen_pos[i]));
      bbox_max = max(bbox_max, int2(screen_pos[i]));
    }
    bbox_min = max(bbox_min, int2{0, 0});
    bbox_max = min(bbox_max, int2{static_cast<int>(fb.color.width - 1),
                                  static_cast<int>(fb.color.height - 1)});
    for (int x = bbox_min.x; x <= bbox_max.x; ++x) {
      for (int y = bbox_min.y; y <= bbox_max.y; ++y) {
        int pixel_idx = y * fb.color.width + x;
        // ref: https://github.com/ssloy/tinyrenderer/blob/master/our_gl.cpp
        float3 bc_screen = barycentric(screen_pos, static_cast<float>(x),
                                       static_cast<float>(y));
        if (bc_screen.x < 0 || bc_screen.y < 0 || bc_screen.z < 0) {
          continue;
        }
        float3 bc_clip = {bc_screen.x / vertex_pos[0].w,
                          bc_screen.y / vertex_pos[1].w,
                          bc_screen.z / vertex_pos[2].w};
        float depth = vertex_pos[0].z * bc_clip[0] +
                      vertex_pos[1].z * bc_clip[1] +
                      vertex_pos[2].z * bc_clip[2];
        bc_clip = bc_clip / (bc_clip[0] + bc_clip[1] + bc_clip[2]);
        if (depth > fb.depth[pixel_idx] || depth < 0.0f) {
          continue;
        }
        // varying
        program->set_frag_depth(depth);
        program->vary(bc_clip);
        float4 color = float4(0.0f);

        if (!program->fragment(color)) {
          // discarded
          continue;
        }
        // blend(src_alpha, one_minus_src_alpha)
        color = lerp(fb.color.data[pixel_idx], color, color.w);
        fb.depth[pixel_idx] = depth;
        fb.color.data[pixel_idx] = color;
      }
    }
  }
}

void context::save_framebuffer(uint8_t *data) { fb.save_to_rgba_uint8(data); }

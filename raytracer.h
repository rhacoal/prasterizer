#ifndef PRASTERIZER_RAYTRACER_H
#define PRASTERIZER_RAYTRACER_H
#include "common.h"

#include <cstddef>
#include <functional>
#include <memory>

typedef uint64_t traversable_handle;

struct geometry_as_build_options {
  uint32_t sbt_offset;
  float3x4 transform;
  const facet_geometry *geometry;
};

struct instance {
  uint32_t sbt_offset;
  uint32_t instance_id;
  float3x4 transform;
  traversable_handle handle;
};

struct instance_as_build_options {
  instance *instances;
  uint32_t instance_count;
};

struct intersection {
  float3 position;
  float3 barycentric;
  traversable_handle handle;
  uint32_t instance_id;
  uint32_t sbt_index;
  uint32_t primitive_index;
  float distance;
  // false if the ray is from outside or true if the ray is from inside
  uint32_t side;
  float padding;
};

struct traverse_context;

void set_per_ray_ptr(traverse_context *ctx, void *ptr);

void *get_per_ray_ptr(traverse_context *ctx);

intersection *get_intersection(traverse_context *ctx);

void *get_launch_params(traverse_context *ctx);

float4x4 get_world_transform(traverse_context *ctx);

float3x3 get_normal_transform(traverse_context *ctx);

float3 get_ray_direction(traverse_context *ctx);

void launch_ray(traverse_context *ctx, traversable_handle handle, float3 origin,
                float3 direction, float tmin, float tmax);

struct hit_record {
  void (*closest_hit)(traverse_context *ctx, void *payload);
  void *payload;
};

struct miss_record {
  void (*miss)(traverse_context *ctx, void *payload);
  void *payload;
};

struct shader_binding_table {
  void (*raygen)(traverse_context *ctx, void *launch_params, uint2 index);
  hit_record *hits;
  uint32_t hit_record_count;
  miss_record miss;
};

struct rt_context_impl;

class rt_context {
public:
  rt_context();
  ~rt_context();
  traversable_handle build_geometry_as(geometry_as_build_options *options);
  traversable_handle build_instance_as(instance_as_build_options *options);
  void update_instance_as(traversable_handle handle,
                          instance_as_build_options *options);

  void launch(void *launch_params, shader_binding_table *sbt, uint32_t width,
              uint32_t height);

private:
  std::unique_ptr<rt_context_impl> impl;
};

struct rt_texture_offset {
  uint32_t offset;
  int width, height;
};

struct rt_vertex {
  float3 position;
  float3 normal;
  float3 tangent;
  float2 texcoord;
  float2 padding;
};

struct ray {
  float3 origin;
  float3 direction;
};
#endif

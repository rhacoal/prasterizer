#include "model.h"
#include "stb_image.h"

#include <filesystem>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>

facet_geometry load_geometry(const std::filesystem::path &p) {
  std::ifstream in(p);
  return load_geometry(in);
}

facet_geometry load_geometry(std::istream &stream) {
  using namespace std::string_view_literals;
  if (!stream) {
    throw std::runtime_error("invalid geometry file");
  }
  std::string line, prefix;
  std::vector<float3> v;
  std::vector<float2> vt;
  std::vector<float3> vn;
  facet_geometry geom;
  while (stream) {
    std::getline(stream, line);
    std::istringstream iss(line);
    if (!(iss >> prefix)) {
      continue;
    }
    if (prefix == "v"sv) {
      float3 p;
      iss >> p.x >> p.y >> p.z;
      v.push_back(p);
    } else if (prefix == "vt"sv) {
      float2 p;
      iss >> p.x >> p.y;
      vt.push_back(p);
    } else if (prefix == "vn"sv) {
      float3 p;
      iss >> p.x >> p.y >> p.z;
      vn.push_back(p);
    } else if (prefix == "f"sv) {
      char sep;
      int a, b, c;
      for (int i = 0; i < 3; ++i) {
        iss >> a >> sep >> b >> sep >> c;
        if (a > v.size() || b > vt.size() || c > vn.size()) {
          throw std::runtime_error("index out of range");
        }
        geom.position.push_back(v[a - 1]);
        geom.texcoord.push_back(vt[b - 1]);
        geom.normal.push_back(vn[c - 1]);
      }
    }
  }
  return geom;
}

texture load_texture(const std::filesystem::path &p, TextureMode mode) {
  std::ifstream in(p, std::ios_base::binary);
  return load_texture(in, mode);
}

texture load_texture(std::istream &stream, TextureMode mode) {
  std::vector<unsigned char> content(std::istreambuf_iterator<char>(stream),
                                     {});
  int w, h, ch;
  float *data = stbi_loadf_from_memory(
      content.data(), static_cast<int>(content.size()), &w, &h, &ch, 0);
  if (data == nullptr || ch < 3) {
    throw std::runtime_error("invalid texture file");
  }
  auto colors = new float4[w * h];
  for (int i = 0; i < h; ++i) {
    for (int j = 0; j < w; ++j) {
      int pixel_index = (i * w + j);
      int offset = pixel_index * ch;
      float r = data[offset];
      float g = data[offset + 1];
      float b = data[offset + 2];
      float a = ch == 4 ? data[offset + 3] : 1.0f;
      float3 color{r, g, b};
      if (mode == TextureMode::SRGB) {
        color = pow(color, 2.2f);
      }
      colors[pixel_index] = float4(color, a);
    }
  }
  stbi_image_free(data);
  texture t;

  return {static_cast<uint32_t>(w), static_cast<uint32_t>(h),
          std::unique_ptr<float4[]>(colors)};
}

texture create_color_texture(float4 color, uint32_t width, uint32_t height) {
  return texture{width, height, [&]() {
                   size_t pixelCount = static_cast<size_t>(width) * height;
                   auto p = std::make_unique<float4[]>(pixelCount);
                   std::fill_n(p.get(), pixelCount, color);
                   return p;
                 }()};
}

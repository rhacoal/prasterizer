#include "raytracer.h"
#include "model.h"
#include "rt_structure.h"

#include "BS_thread_pool_light.hpp"

#include <algorithm>
#include <cmath>
#include <deque>
#include <stdexcept>
#include <variant>

bool intersect(float3 origin, float3 direction, float3 p[3],
               intersection &intersection) {
  // Reference:
  // https://scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/moller-trumbore-ray-triangle-intersection

  float3 e01 = p[1] - p[0];
  float3 e02 = p[2] - p[0];

  float3 pvec = cross(direction, e02);
  float det = dot(e01, pvec);
  if (std::fabs(det) < 1e-8f)
    return false;
  float invDet = 1 / det;

  float3 tvec = origin - p[0];
  float u = dot(tvec, pvec) * invDet;
  if (u < 0 || u > 1)
    return false;

  float3 qvec = cross(tvec, e01);
  float v = dot(direction, qvec) * invDet;
  if (v < 0 || u + v > 1)
    return false;

  float t = dot(e02, qvec) * invDet;
  if (t < 0)
    return false;

  intersection.barycentric = float3{1 - u - v, u, v};
  intersection.distance = t;
  intersection.position = origin + t * direction;
  intersection.side = det < 0;
  return true;
}

bool bounds_ray_intersects(float3 origin, float3 direction, bounds3 aabb,
                           float *tmin_out, float *tmax_out) {
  float3 inverse_ray = 1.0f / direction;
  // Reference:
  // https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection
  float3 bounds[2] = {aabb.p_min, aabb.p_max};
  float tmin, tmax, tymin, tymax, tzmin, tzmax;

  int sign[3];
  sign[0] = direction.x > 0;
  sign[1] = direction.y > 0;
  sign[2] = direction.z > 0;

  tmin = (bounds[1 - sign[0]].x - origin.x) * inverse_ray.x;
  tmax = (bounds[sign[0]].x - origin.x) * inverse_ray.x;
  tymin = (bounds[1 - sign[1]].y - origin.y) * inverse_ray.y;
  tymax = (bounds[sign[1]].y - origin.y) * inverse_ray.y;

  if ((tmin > tymax) || (tymin > tmax)) {
    return false;
  }
  if (tymin > tmin) {
    tmin = tymin;
  }
  if (tymax < tmax) {
    tmax = tymax;
  }

  tzmin = (bounds[1 - sign[2]].z - origin.z) * inverse_ray.z;
  tzmax = (bounds[sign[2]].z - origin.z) * inverse_ray.z;

  if ((tmin > tzmax) || (tzmin > tmax)) {
    return false;
  }
  if (tzmin > tmin) {
    tmin = tzmin;
  }
  if (tzmax < tmax) {
    tmax = tzmax;
  }
  *tmin_out = tmin;
  *tmax_out = tmax;

  return true;
}

struct geometry_payload {
  std::vector<std::pair<uint32_t, std::array<float3, 3>>> positions;
  uint32_t sbt_offset;
  float3x4 transform;
};

struct instance_payload {
  std::vector<instance> instances;
};

struct traversable {
  std::vector<bvh_node> bvh;
  std::variant<geometry_payload, instance_payload> primitives;
};

struct traverse_context {
  shader_binding_table *sbt;
  void *launch_params;
  float4x4 matrix;
  float3x3 normal_matrix;
  float3 ray_origin;
  float3 ray_direction;
  void *per_ray;
  intersection hit;
  bool has_hit;
  uint32_t instance_id;
  uint32_t instance_sbt_offset;
};

void set_per_ray_ptr(traverse_context *ctx, void *ptr) { ctx->per_ray = ptr; }

void *get_per_ray_ptr(traverse_context *ctx) { return ctx->per_ray; }

intersection *get_intersection(traverse_context *ctx) { return &(ctx->hit); }

void *get_launch_params(traverse_context *ctx) { return ctx->launch_params; }

float4x4 get_world_transform(traverse_context *ctx) { return ctx->matrix; }

float3x3 get_normal_transform(traverse_context *ctx) {
  return ctx->normal_matrix;
}

float3 get_ray_direction(traverse_context *ctx) {
  return normalize(ctx->ray_direction);
}

void launch_ray(traverse_context *ctx, traversable_handle handle,
                float3 origin0, float3 direction0, float tmin, float tmax) {
  // traverse
  float4x4 matrix0{linalg::identity};
  ctx->has_hit = false;
  ctx->instance_id = 0;
  ctx->instance_sbt_offset = 0;
  // find leaf node
  const auto traverse = [&](auto &&traverse, traversable_handle as_handle,
                            float4x4 matrix) -> void {
    auto as = reinterpret_cast<traversable *>(as_handle);
    if (as->bvh.empty()) {
      throw std::runtime_error("invalid acceleration structure");
    }

    if (as->primitives.index() == 0) {
      auto &geometry = std::get<0>(as->primitives);
      float4x4 gas_matrix{
          float4{geometry.transform[0], 0.0f},
          float4{geometry.transform[1], 0.0f},
          float4{geometry.transform[2], 0.0f},
          float4{geometry.transform[3], 1.0f},
      };
      matrix = mul(matrix, gas_matrix);
    }

    uint32_t stack[64];
    uint32_t stackSize = 1;
    float3x3 normal_matrix = transpose(
        inverse(float3x3{matrix.x.xyz(), matrix.y.xyz(), matrix.z.xyz()}));
    auto inv = inverse(matrix);
    auto origin = mul(inv, float4(origin0, 1.0f)).xyz();
    auto direction = mul(inv, float4(direction0, 0.0f)).xyz();
    auto dir_len = length(direction);
    direction /= dir_len;
    stack[0] = 0;
    while (stackSize) {
      uint32_t node_idx = stack[--stackSize];
      bvh_node &node = as->bvh[node_idx];
      if (node.is_leaf) {
        if (as->primitives.index() == 0) {
          // geometry
          auto &geometry = std::get<0>(as->primitives);
          intersection hit{};
          if (intersect(origin, direction,
                        geometry.positions[node.offset].second.data(), hit)) {
            hit.distance /= dir_len;
            if (hit.distance < tmin) {
              // continue
              int a = 0;
            } else if (hit.distance < tmax) {
              tmax = hit.distance;
              hit.sbt_index = geometry.sbt_offset + ctx->instance_sbt_offset;
              hit.instance_id = ctx->instance_id;
              hit.handle = as_handle;
              hit.primitive_index = geometry.positions[node.offset].first;
              ctx->hit = hit;
              ctx->has_hit = true;
              ctx->matrix = matrix;
              ctx->normal_matrix = normal_matrix;
              ctx->ray_origin = origin0;
              ctx->ray_direction = direction0;
            }
          }
        } else {
          // instances
          auto &ias = std::get<1>(as->primitives);
          ias.instances[node.offset];
          ctx->instance_id = node.offset;
          ctx->instance_sbt_offset = ias.instances[node.offset].sbt_offset;
          float4x4 instance_matrix{
              float4{ias.instances[node.offset].transform[0], 0.0f},
              float4{ias.instances[node.offset].transform[1], 0.0f},
              float4{ias.instances[node.offset].transform[2], 0.0f},
              float4{ias.instances[node.offset].transform[3], 1.0f},
          };
          traverse(traverse, ias.instances[node.offset].handle,
                   mul(matrix, instance_matrix));
          // for (uint32_t i = 0; i < ias.instances.size(); ++i) {
          //   ctx->instance_id = i;
          //   ctx->instance_sbt_offset = ias.instances[i].sbt_offset;
          //   float4x4 instance_matrix{
          //       float4{ias.instances[i].transform[0], 0.0f},
          //       float4{ias.instances[i].transform[1], 0.0f},
          //       float4{ias.instances[i].transform[2], 0.0f},
          //       float4{ias.instances[i].transform[3], 1.0f},
          //   };
          //   traverse(traverse, ias.instances[i].handle,
          //            mul(matrix, instance_matrix));
          // }
        }
      } else {
        int sign[3];
        sign[0] = direction.x > 0;
        sign[1] = direction.y > 0;
        sign[2] = direction.z > 0;

        uint32_t t[2] = {node_idx + 1, node.offset};
        float box_tmin, box_tmax;
        if (bounds_ray_intersects(origin, direction,
                                  as->bvh[t[sign[node.dim]]].bounds, &box_tmin,
                                  &box_tmax)) {
          box_tmin /= dir_len;
          box_tmax /= dir_len;
          if (box_tmin <= tmax && box_tmax >= tmin) {
            stack[stackSize++] = t[sign[node.dim]];
          }
        }
        if (bounds_ray_intersects(origin, direction,
                                  as->bvh[t[1 - sign[node.dim]]].bounds,
                                  &box_tmin, &box_tmax)) {
          box_tmin /= dir_len;
          box_tmax /= dir_len;
          if (box_tmin <= tmax && box_tmax >= tmin) {
            stack[stackSize++] = t[1 - sign[node.dim]];
          }
        }
      }
      if (stackSize >= 63) {
        // prevent stack overflow
        break;
      }
    }
  };
  traverse(traverse, handle, matrix0);
  if (ctx->has_hit) {
    auto &record = ctx->sbt->hits[ctx->hit.sbt_index];
    record.closest_hit(ctx, record.payload);
  } else {
    auto &record = ctx->sbt->miss;
    ctx->ray_origin = origin0;
    ctx->ray_direction = direction0;
    ctx->matrix = float4x4(linalg::identity);
    record.miss(ctx, record.payload);
  }
}

bounds3 bounds_of(const std::pair<uint32_t, std::array<float3, 3>> &triangle) {
  bounds3 b3(triangle.second[0]);
  b3 += triangle.second[1];
  b3 += triangle.second[2];
  return b3;
}

bounds3 bounds_of(const instance &inst) {
  auto ib3 = reinterpret_cast<const traversable *>(inst.handle)->bvh[0].bounds;
  bounds3 b3{};
  float3 ps[] = {ib3.p_min, ib3.p_max};
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      for (int k = 0; k < 2; ++k) {
        b3 += mul(inst.transform, float4{ps[i].x, ps[j].y, ps[k].z, 1.0f});
      }
    }
  }
  return b3;
}

template <typename T, typename... Args>
static uint32_t push(std::vector<T> &nodes, Args &&...args) {
  uint32_t ret = static_cast<uint32_t>(nodes.size());
  nodes.emplace_back(std::forward<Args>(args)...);
  return ret;
};

template <typename It>
uint32_t recur(std::vector<bvh_node> &nodes, It first, It last,
               uint32_t base_index) {
  uint32_t length = static_cast<uint32_t>(last - first);
  if (length == 1) {
    return push(nodes, bvh_node{bounds_of(*first), base_index, 1}); // leaf
  }
  if (length == 2) {
    bounds3 bounds = bounds_of(*first) + bounds_of(*std::next(first));
    uint8_t dim = bounds.maxExtent();
    uint32_t ret = push(nodes, bvh_node{bounds, 0, 0, dim});
    if (bounds_of(*first).centroid()[dim] <
        bounds_of(*std::next(first)).centroid()[dim]) {
      // left
      recur(nodes, first, std::next(first), base_index);
      // right
      nodes[ret].offset = recur(nodes, std::next(first), last, base_index + 1);
    } else {
      // right
      recur(nodes, std::next(first), last, base_index + 1);
      // left
      nodes[ret].offset = recur(nodes, first, std::next(first), base_index);
    }
    return ret;
  }
  bounds3 bounds = bounds_of(*first);
  bounds3 centroid_bounds = bounds3(bounds.centroid());
  for (auto i = std::next(first); i != last; ++i) {
    bounds3 i_bounds = bounds_of(*i);
    bounds += i_bounds;
    centroid_bounds += i_bounds.centroid();
  }
  uint8_t maxExtend = bounds.maxExtent();
  uint32_t leftSize = length / 2;
  std::nth_element(first, first + leftSize, last,
                   [maxExtend](const auto &a, const auto &b) -> bool {
                     return bounds_of(a).centroid()[maxExtend] <
                            bounds_of(b).centroid()[maxExtend];
                   });

  auto ret = push(nodes, bvh_node{bounds, 0, 0, maxExtend});
  recur(nodes, first, first + leftSize, base_index);
  nodes[ret].offset =
      recur(nodes, first + leftSize, last, base_index + leftSize);
  return ret;
};

template <typename T>
std::vector<bvh_node> build_bvh(std::vector<T> &triangles) {
  std::vector<bvh_node> nodes;
  // prevent empty buffer
  if (triangles.empty()) {
    nodes.push_back({bounds3(float3{})});
  } else {
    recur(nodes, triangles.begin(), triangles.end(), 0u);
  }
  return nodes;
}

struct rt_context_impl {
  rt_context_impl() {}
  traversable_handle build_geometry_as(geometry_as_build_options *options) {
    size_t vertex_count = options->geometry->position.size() / 3;
    std::vector<std::pair<uint32_t, std::array<float3, 3>>> triangles;
    triangles.reserve(vertex_count);
    for (uint32_t i = 0; i < vertex_count; ++i) {
      auto it = options->geometry->position.data() + i * 3;
      triangles.push_back({i, {it[0], it[1], it[2]}});
    }
    auto nodes = build_bvh(triangles);
    traversables.push_back(traversable{
        std::move(nodes),
        std::variant<geometry_payload, instance_payload>{
            std::in_place_index<0>,
            geometry_payload{std::move(triangles), options->sbt_offset,
                             options->transform}}});
    return reinterpret_cast<traversable_handle>(&traversables.back());
  }

  traversable_handle build_instance_as(instance_as_build_options *options) {
    std::vector<instance> instances(
        options->instances, options->instances + options->instance_count);
    auto nodes = build_bvh(instances);
    traversables.push_back(traversable{
        std::move(nodes),
        std::variant<geometry_payload, instance_payload>{
            std::in_place_index<1>, instance_payload{std::move(instances)}}});
    return reinterpret_cast<traversable_handle>(&traversables.back());
  }

  void update_instance_as(traversable_handle handle,
                          instance_as_build_options *options) {
    // find previous traversable
    auto tr = reinterpret_cast<traversable *>(handle);
    std::vector<instance> instances(
        options->instances, options->instances + options->instance_count);
    tr->bvh = build_bvh(instances);
    tr->primitives = std::variant<geometry_payload, instance_payload>{
        std::in_place_index<1>, instance_payload{std::move(instances)}};
  }

  void launch(void *launch_params, shader_binding_table *sbt, uint32_t width,
              uint32_t height) {
    pool.push_loop(
        height,
        [=](uint32_t y0, uint32_t y1) {
          for (uint32_t y = y0; y < y1; ++y) {
            for (uint32_t x = 0; x < width; ++x) {
              traverse_context ctx{sbt, launch_params,
                                   float4x4(linalg::identity),
                                   float3x3(linalg::identity)};
              sbt->raygen(&ctx, launch_params, {x, y});
            }
          }
        },
        height);
    pool.wait_for_tasks();
  }

  std::deque<traversable> traversables;
  BS::thread_pool_light pool;
};

rt_context::rt_context() : impl(std::make_unique<rt_context_impl>()) {}
rt_context::~rt_context() {}

traversable_handle
rt_context::build_geometry_as(geometry_as_build_options *options) {
  return impl->build_geometry_as(options);
}

traversable_handle
rt_context::build_instance_as(instance_as_build_options *options) {
  return impl->build_instance_as(options);
}

void rt_context::update_instance_as(traversable_handle handle,
                                    instance_as_build_options *options) {
  impl->update_instance_as(handle, options);
}

void rt_context::launch(void *launch_params, shader_binding_table *sbt,
                        uint32_t width, uint32_t height) {
  impl->launch(launch_params, sbt, width, height);
}

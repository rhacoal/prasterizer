#ifndef PRASTERIZER_PROGRAMS_H
#define PRASTERIZER_PROGRAMS_H
#include "rasterizer.h"
#include "raytracer.h"
#include "scene.h"
#include "shader.h"
#include <random>

namespace math {
template <typename F = double> constexpr F pi() {
  return F(3.14159265358979323846);
}

template <typename F = double> constexpr F inv_pi() { return F(1.0) / pi<F>(); }

template <typename F> constexpr F radians(F degree) {
  return degree / F(180.) * pi<F>();
}

inline float3 reflect(const float3 &I, const float3 &N) {
  return I - 2.0f * dot(I, N) * N;
}
} // namespace math

class phong : public shader {
public:
  virtual float4 vertex(const vertex_attributes &attrib) override {
    float4 pos(attrib.position, 1.0f);

    float3 &v_position = varying3f(0);
    float3 &v_normal = varying3f(1);
    float2 &v_texcoord = varying2f(2);

    v_position = mul(m, pos).xyz();
    v_normal = mul(m_it, attrib.normal);
    v_texcoord = attrib.texcoord;

    pos = mul(mvp, pos);
    return pos;
  }

  virtual bool fragment(float4 &color) override {
    float3 position = varying3f(0);
    float3 normal = linalg::normalize(varying3f(1));
    float2 texcoord = varying2f(2);
    float4 materialDiffuse = sample(*texAlbedo, texcoord);
    float3 diffuse = materialDiffuse.xyz() * colorDiffuse;
    float3 specular = materialDiffuse.xyz() * colorSpecular;
    float alpha = materialDiffuse.w;

    float3 lightVector = light.position - position;
    float lightDistance = length(lightVector);
    float3 L = lightVector / lightDistance;
    float3 V = normalize(cameraPosition - position);
    float3 H = normalize(L + V);
    float diffusePower = std::clamp(dot(normal, L), 0.0f, 1.0f);
    float specularPower =
        std::pow(std::clamp(dot(normal, H), 0.0f, 1.0f), shininess);
    float3 out = (diffusePower * diffuse + specularPower * specular) *
                     light.color * (1.0f / (lightDistance * lightDistance)) +
                 colorAmbient * diffuse + colorEmission;
    // gamma correction
    color = float4(pow(out, 1.0f / 2.2f), alpha);
    return true;
  }

  // uniforms
  float4x4 m;
  float4x4 mv;
  float3x3 m_it;
  float4x4 mvp;
  texture *texAlbedo;
  float3 cameraPosition;
  float3 colorEmission;
  struct PointLight {
    float3 position;
    float3 color;
  } light;
  float3 colorSpecular;
  float3 colorDiffuse;
  float3 colorAmbient;
  float shininess;
};

class envmap_shader : public shader {
public:
  virtual float4 vertex(const vertex_attributes &attrib) override {
    float3 &v_position = varying3f(0);
    v_position = attrib.position;
    float4 position = mul(vp, float4(attrib.position, 1.0f));
    position.z = position.w + 0.0001f;
    return position;
  }

  virtual bool fragment(float4 &color) override {
    float3 dir = -normalize(varying3f(0));
    float u = 0.5f + std::atan2(dir.x, -dir.z) / (2.0f * math::pi<float>());
    float v = 0.5f + std::asin(dir.y) / math::pi<float>();
    float3 color3 = sample(*tex, float2{u, v}).xyz();
    color3 = pow(color3, 1.0f / 2.2f);
    color = float4(color3, 1.0f);
    return true;
  }

  // uniforms
  float4x4 vp;
  texture *tex;
};

inline float3x4 convert(float4x4 f) {
  return {f.x.xyz(), f.y.xyz(), f.z.xyz(), f.w.xyz()};
}

struct ray_data {
  uint32_t x, y;
  uint32_t pixel_id;
  float pdf;
  float3 color;
  uint32_t depth;
};

struct launch {
  float3 camera_pos;
  float3 camera_dir;
  float3 camera_up;
  float fov;
  float near;
  float far;
  uint32_t width;
  uint32_t height;
  uint32_t samples;
  framebuffer *fb;
  texture *envmap;
  float envmap_intensity;
  std::vector<ray_data> prd;

  float aspect;
  float top;
  float inv_w2;
  float inv_h2;
  float3 camera_x;
  float3 camera_y;

  traversable_handle handle;
};

void ray_closest_hit(traverse_context *ctx, void *payload);

void ray_miss(traverse_context *ctx, void *payload);

void raygen(traverse_context *ctx, void *launch_params, uint2 index);
#endif

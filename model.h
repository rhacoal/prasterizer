#ifndef PRASTERIZER_MODEL_H
#define PRASTERIZER_MODEL_H
#include "common.h"

#include <filesystem>
#include <memory>
#include <vector>

struct facet_geometry {
  std::vector<float3> position;
  std::vector<float2> texcoord;
  std::vector<float3> normal;
};

struct texture {
  uint32_t width, height;
  std::unique_ptr<float4[]> data;
};

enum class TextureMode {
  LINEAR, SRGB,
};

facet_geometry load_geometry(const std::filesystem::path &p);

facet_geometry load_geometry(std::istream &stream);

texture load_texture(const std::filesystem::path &p, TextureMode mode = TextureMode::LINEAR);

texture load_texture(std::istream &stream, TextureMode mode = TextureMode::LINEAR);

texture create_color_texture(float4 color, uint32_t width = 1, uint32_t height = 1);
#endif

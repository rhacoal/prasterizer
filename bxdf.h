// Copyright Disney Enterprises, Inc.  All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License
// and the following modification to it: Section 6 Trademarks.
// deleted and replaced with:
//
// 6. Trademarks. This License does not grant permission to use the
// trade names, trademarks, service marks, or product names of the
// Licensor and its affiliates, except as required for reproducing
// the content of the NOTICE file.
//
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0

#ifndef PRASTERIZER_BXDF_H
#define PRASTERIZER_BXDF_H

#include "common.h"
#include <cmath>

#define RT_M_PI_F 3.14159274101257f
#define RT_M_1_PI_F 0.31830987334251f

float sqr(float x) { return x * x; }

float SchlickFresnel(float u) {
  float m = std::clamp(1.0f - u, 0.0f, 1.0f);
  float m2 = m * m;
  return m2 * m2 * m;
}

float IorToR0(float VdotH, float eta) {
  // use 1.0f as R90
  float sinVH = 1.0f - sqr(VdotH);
  float sinLH2 = eta * eta * sinVH;
  if (sinLH2 >= 1) {
    return 1.0f;
  }
  return sqr((eta - 1.0f) / (eta + 1.0f));
}

float GTR2(float NdotH, float a) {
  float a2 = a * a;
  float t = 1.0f + (a2 - 1.0f) * NdotH * NdotH;
  return a2 / (RT_M_PI_F * t * t);
}

float smithG_GGX(float NdotV, float alphaG) {
  float a = alphaG * alphaG;
  float b = NdotV * NdotV;
  return 1.0f / (NdotV + sqrt(a + b - a * b));
}

float3 DiffuseBRDF(float3 L, float3 V, float3 N, float roughness,
                   float3 diffuseColor) {
  float NdotL = dot(N, L);
  float NdotV = dot(N, V);
  if (NdotV <= 0.0f || NdotL <= 0.0f)
    return float3(0.0f);
  float3 H = normalize(L + V);
  float LdotH = dot(L, H);
  // diffuse fresnel
  float FL = SchlickFresnel(NdotL), FV = SchlickFresnel(NdotV);
  float Fd90 = 0.5f + 2 * LdotH * LdotH * roughness;
  float Fd = linalg::lerp(1.0f, Fd90, FL) * linalg::lerp(1.0f, Fd90, FV);
  return RT_M_1_PI_F * Fd * diffuseColor;
}

float3 SpecularBRDF(float3 L, float3 V, float3 N, float roughness, float3 R0) {
  float NdotV = dot(N, V);
  float NdotL = dot(N, L);
  if (NdotV <= 0.0f || NdotL <= 0.0f)
    return float3(0.0f);
  float3 H = normalize(L + V);
  float LdotH = dot(L, H);
  float NdotH = dot(N, H);
  float a = std::max(.01f, sqr(roughness));
  float Ds = GTR2(NdotH, a);
  float FH = SchlickFresnel(LdotH);
  float3 Fs = lerp(R0, float3(1.0f), FH);
  float Gs = smithG_GGX(NdotV, a);
  return Gs * Fs * Ds;
}

float BsdfNDot(float3 v) { return v.y; }

float SmithGGXMasking(float3 wi, float3 wo, float a2) {
  float dotNL = BsdfNDot(wi);
  float dotNV = BsdfNDot(wo);
  float denomC = std::sqrt(a2 + (1.0f - a2) * dotNV * dotNV) + dotNV;

  return 2.0f * dotNV / denomC;
}

float SmithGGXMaskingShadowing(float3 wi, float3 wo, float a2) {
  float dotNL = BsdfNDot(wi);
  float dotNV = BsdfNDot(wo);

  float denomA = dotNV * std::sqrt(a2 + (1.0f - a2) * dotNL * dotNL);
  float denomB = dotNL * std::sqrt(a2 + (1.0f - a2) * dotNV * dotNV);

  return 2.0f * dotNL * dotNV / (denomA + denomB);
}

// https://hal.archives-ouvertes.fr/hal-01509746/document
float3 GgxVndf(float3 wo, float roughness, float u1, float u2) {
  // -- Stretch the view vector so we are sampling as though
  // -- roughness==1
  float3 v = normalize(float3(wo.x * roughness, wo.y, wo.z * roughness));

  // -- Build an orthonormal basis with v, t1, and t2
  float3 t1 = (v.y < 0.999f) ? normalize(cross(v, float3{0.0f, 1.0f, 0.0f}))
                             : float3{1.0f, 0.0f, 0.0f};
  float3 t2 = cross(t1, v);

  // -- Choose a point on a disk with each half of the disk weighted
  // -- proportionally to its projection onto direction v
  float a = 1.0f / (1.0f + v.y);
  float r = std::sqrt(u1);
  float phi = (u2 < a) ? (u2 / a) * math::pi<float>()
                       : math::pi<float>() + (u2 - a) / (1.0f - a) * math::pi<float>();
  float p1 = r * std::cos(phi);
  float p2 = r * std::sin(phi) * ((u2 < a) ? 1.0f : v.y);

  // -- Calculate the normal in this stretched tangent space
  float3 n = p1 * t1 + p2 * t2 +
             std::sqrt(std::max<float>(0.0f, 1.0f - p1 * p1 - p2 * p2)) * v;

  // -- unstretch and normalize the normal
  return normalize(
      float3(roughness * n.x, std::max<float>(0.0f, n.y), roughness * n.z));
}

void ImportanceSampleGgxVdn(float r0, float r1,
                            const float3 &wo, float roughness,
                            const float3 &specularColor, float3 &wi,
                            float3 &reflectance) {
  float a = roughness;
  float a2 = a * a;

  float3 wm = GgxVndf(wo, roughness, r0, r1);

  wi = math::reflect(-wo, wm);

  if (BsdfNDot(wi) > 0.0f) {
    float3 F = lerp(specularColor, float3(1.0f), SchlickFresnel(dot(wi, wm)));
    float G1 = SmithGGXMasking(wi, wo, a2);
    float G2 = SmithGGXMaskingShadowing(wi, wo, a2);
    reflectance = F * (G2 / G1);
  } else {
    reflectance = float3{};
  }
}

float3 BRDF(float3 L, float3 V, float3 N, float roughness, float3 diffuseColor,
            float3 specularR0) {
  return DiffuseBRDF(L, V, N, roughness, diffuseColor) +
         SpecularBRDF(L, V, N, roughness, specularR0);
}
#endif // ASSIGNMENT_BXDF_H

#include "shader.h"
#include "model.h"

#include <algorithm>

static void sample1d(float u, int width, int *x0, int *x1, float *p) {
  u = u - floor(u);
  float x = u * (float)width - 0.5f;
  int px = (int)x;
  *p = x - (float)px;
  *x0 = px + (px < 0) * width;
  *x1 = (px + 1) % width;
}

float4 shader::sample(const texture &tex, const float2 &uv) {
  // TODO: mipmapping
  int x0, y0, x1, y1;
  float xp, yp;
  sample1d(uv.x, tex.width, &x0, &x1, &xp);
  sample1d(1 - uv.y, tex.height, &y0, &y1, &yp);

  // refueses invalid access
  if (x0 < 0 || x1 < 0 || y0 < 0 || y1 < 0) {
    return float4{0.0f};
  }
  if (x0 >= tex.width || x1 >= tex.width || y0 >= tex.height || y1 >= tex.height) {
    return float4{0.0f};
  }
  return lerp(
      lerp(tex.data[x0 + y0 * tex.width], tex.data[x1 + y0 * tex.width], xp),
      lerp(tex.data[x0 + y1 * tex.width], tex.data[x1 + y1 * tex.width], xp),
      yp);
}

struct varying_data {
  static constexpr size_t max_varying = 8;
  std::uint32_t vertex_idx;
  std::byte data[max_varying][4][16]{};
  bool used[max_varying]{};
  float frag_depth{};
};

static thread_local varying_data _varying{};

float shader::frag_depth() const { return _varying.frag_depth; }

static std::byte *varying(uint32_t index) {
  _varying.used[index] = true;
  return _varying.data[index][_varying.vertex_idx] + 0;
}

void shader::use() {
  std::fill_n(_varying.used, std::size(_varying.used), false);
}

float3 polarize(const float3 &f) {
  float bcc[4] = {f.x, f.y, f.z};
  auto t = std::max_element(std::begin(bcc), std::end(bcc)) - std::begin(bcc);
  float3 x{};
  x[t] = 1.0f;
  return x;
}

void shader::vary(const float3 &bar) {
  _varying.vertex_idx = 3;
  for (size_t i = 0; i != varying_data::max_varying; ++i) {
    if (_varying.used[i]) {
      auto &v0 = *reinterpret_cast<float4 *>(_varying.data[i][0] + 0);
      auto &v1 = *reinterpret_cast<float4 *>(_varying.data[i][1] + 0);
      auto &v2 = *reinterpret_cast<float4 *>(_varying.data[i][2] + 0);
      auto &v3 = *reinterpret_cast<float4 *>(_varying.data[i][3] + 0);
      v3 = v0 * bar[0] + v1 * bar[1] + v2 * bar[2];
    }
  }
}

void shader::set_vertex_index(uint32_t vertex_idx) {
  _varying.vertex_idx = vertex_idx;
}

void shader::set_frag_depth(float frag_depth) {
  _varying.frag_depth = frag_depth;
}

float2 &shader::varying2f(uint32_t index) {
  return *reinterpret_cast<float2 *>(varying(index));
}

float3 &shader::varying3f(uint32_t index) {
  return *reinterpret_cast<float3 *>(varying(index));
}

float4 &shader::varying4f(uint32_t index) {
  return *reinterpret_cast<float4 *>(varying(index));
}

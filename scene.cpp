#include "scene.h"

void object::set_position(const float3 &pos) {
  _position = pos;
  update_local_matrix();
}

void object::set_quaternion(const float4 &quat) {
  _quaternion = quat;
  update_local_matrix();
}

void object::set_scale(const float3 &scale_) {
  _scale = scale_;
  update_local_matrix();
}

const float3 &object::position() const { return _position; }

const float4 &object::quaternion() const { return _quaternion; }

const float3 &object::scale() const { return _scale; }

const float4x4 &object::matrix() const { return _local; }

bool object::is_mesh() const { return false; }

void object::update_local_matrix() const {
  _local = mul(pose_matrix(_quaternion, _position), scaling_matrix(_scale));
}

mesh::mesh(facet_geometry geometry, texture diffuse)
    : geometry(std::move(geometry)), diffuse(std::move(diffuse)) {}

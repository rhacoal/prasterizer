#include "model.h"
#include "programs.h"
#include "rasterizer.h"
#include "raytracer.h"
#include "scene.h"
#include "shader.h"
#include "stb_image_write.h"

#include <glad/gl.h>

#include <GLFW/glfw3.h>
#include <cmrc/cmrc.hpp>
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include <cmath>
#include <cstring>
#include <memory>
#include <sstream>
#include <iostream>

CMRC_DECLARE(resources);

struct app_main {
  app_main(GLFWwindow *window, uint32_t width, uint32_t height)
      : window(window), width(width), height(height) {}

  void init() {
    // init scene
    init_scene();
    // init rt context
    init_rt();
    // init glfw callbacks
    glfwSetWindowUserPointer(window, this);
    // resize buffer when glfw windows resizes
    glfwSetFramebufferSizeCallback(window, [](GLFWwindow *window, int w,
                                              int h) {
      auto app = reinterpret_cast<app_main *>(glfwGetWindowUserPointer(window));
      app->resize(w, h);
    });
    resize(width, height);
    // init imgui
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();
    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 330 core");
    // init OpenGL buffers
    glGenTextures(1, &image);
    glBindTexture(GL_TEXTURE_2D, image);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA,
                 GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
    // init fbo
    glGenFramebuffers(1, &readFboId);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, readFboId);
    glFramebufferTexture2D(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                           GL_TEXTURE_2D, image, 0);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
  }

  void update() {
    update_camera();
    bool need_update_instance_as = false;
    if (rotate) {
      float delta = ImGui::GetIO().DeltaTime;
      rotated_radians =
          std::fmod(delta * math::pi<float>() / 2.0f + rotated_radians,
                    math::pi<float>() * 2);
      // printf("rotating to %f\n", rotated_radians);
      float4 q = rotation_quat(float3(0.0f, 1.0f, 0.0f), rotated_radians);
      bunny->set_quaternion(q);
      need_update_instance_as = true;
    }
    if (mode == 1 && need_update_instance_as) {
      invalidate_samples = true;
      need_update_instance_as = false;
      update_instance_as();
    }
  }

  void draw() {
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
    ImGui::Begin("Config");
    ImGui::RadioButton("Rasterization", &mode, 0);
    ImGui::RadioButton("Ray tracing", &mode, 1);
    ImGui::Text("average %.3f ms/%s (%.1f FPS)",
                1000.0f / ImGui::GetIO().Framerate, mode == 0 ? "frame" : "spp",
                ImGui::GetIO().Framerate);
    ImGui::Checkbox("Rotate", &rotate);
    ImGui::DragFloat("FOV", &camera.fov, 0.01f, 0.01f, 1.0f);
    char filename[64];
    if (mode == 0) {
      snprintf(filename, std::size(filename), "output.png");
    } else {
      snprintf(filename, std::size(filename), "output-%u.png", params.samples);
    }
    if (ImGui::Button("save to file") ||
        (params.samples % 1024 == 0 && params.samples > 0)) {
      std::vector<uint8_t> data(ctx.width() * ctx.height() * 4);
      ctx.save_framebuffer(data.data());
      stbi_write_png(filename, ctx.width(), ctx.height(), 4, data.data(), 0);
    }
    if (params.samples == 10240) {
      should_exit_flag = true;
    }
    ImGui::SameLine();
    ImGui::Text("%s", filename);
    if (mode == 1) {
      ImGui::Text("accumulated spp: %u", params.samples);
    } else {
      ImGui::Text("");
    }
    ImGui::Text("camera pos: (%.3f, %.3f, %.3f)", camera.pos.x, camera.pos.y,
                camera.pos.z);
    ImGui::Text("camera dir: (%.3f, %.3f, %.3f)", camera.dir.x, camera.dir.y,
                camera.dir.z);
    ImGui::Text("framebuffer size: (%u, %u)", width, height);
    ImGui::End();
    if (mode == 0) {
      // Rasterization
      ctx.clear_color(0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
      auto v = linalg::lookat_matrix(camera.pos, camera.pos + camera.dir,
                                     float3{0.0f, 1.0f, 0.0f});
      auto p = linalg::perspective_matrix(
          camera.fov, static_cast<float>(width) / static_cast<float>(height), 0.01f,
          100.0f);
      // draw envmap
      envmap_program.tex = envmap.get();
      auto env_view = v;
      env_view.w = float4{0.0f, 0.0f, 0.0f, 1.0f};
      envmap_program.vp = mul(p, env_view);
      ctx.draw_facets(&envmap_program, skybox.get());
      // draw objects
      program.light = phong::PointLight{
          float3{0.0f, 0.5f, -0.9f},
          float3{10.0f, 10.0f, 0.2f},
      };
      program.colorSpecular = float3{0.1f};
      program.cameraPosition = camera.pos;
      for (auto *obj : scene) {
        auto m = obj->matrix();
        program.texAlbedo = &obj->diffuse;
        program.m = m;
        program.mv = mul(v, m);
        program.m_it = linalg::transpose(
            linalg::inverse(float3x3{m.x.xyz(), m.y.xyz(), m.z.xyz()}));
        program.mvp = mul(p, program.mv);
        program.colorDiffuse = float3{std::max(1.0f - obj->metallic, 0.3f)};
        program.colorAmbient = float3{0.4f, 0.4f, 0.4f};
        program.colorEmission = obj->emission;
        program.colorSpecular = float3{std::max(0.04f, obj->metallic)};
        program.shininess =
            std::clamp(2.0f / std::pow(obj->roughness, 4.0f) - 2.0f, 1.0f, 1000.0f);
        ctx.draw_facets(&program, &obj->geometry);
      };
    } else if (mode == 1) {
      // Ray tracing
      params.camera_pos = camera.pos;
      params.camera_dir = camera.dir;
      params.camera_up = camera.up;
      params.fov = camera.fov;
      params.near = 0.01f;
      params.far = 100.0f;
      if (params.width != ctx.width() || params.height != ctx.height()) {
        invalidate_samples = true;
      }
      params.width = ctx.width();
      params.height = ctx.height();
      params.fb = &ctx.frambuffer();
      params.envmap = envmap.get();
      params.envmap_intensity = 1.0f;
      params.prd.resize(ctx.width() * ctx.height());
      params.aspect = (float)params.width / (float)params.height;
      params.top = params.near * tan(params.fov / 2);
      params.inv_w2 = 2.0f / (float)params.width;
      params.inv_h2 = 2.0f / (float)params.height;
      params.camera_x = normalize(cross(params.camera_dir, params.camera_up));
      params.camera_y = cross(params.camera_x, params.camera_dir);
      params.handle = root_handle;
      if (invalidate_samples || prev_camera_params != camera) {
        invalidate_samples = false;
        params.samples = 0;
        prev_camera_params = camera;
        rt_ctx.launch(&params, &clear_samples, params.width, params.height);
      }
      rt_ctx.launch(&params, &sbt, params.width, params.height);
      params.samples += 1;
    }
    auto &fb = ctx.frambuffer().color;
    glBindTexture(GL_TEXTURE_2D, image);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, fb.width, fb.height, 0, GL_RGBA,
                 GL_FLOAT, fb.data.get());
    glBindTexture(GL_TEXTURE_2D, 0);
    // print opengl errors
    GLenum err;
    while ((err = glGetError()) != GL_NO_ERROR) {
      fprintf(stderr, "OpenGL error: %d\n", err);
    }
    // draws to main window
    glBindFramebuffer(GL_READ_FRAMEBUFFER, readFboId);
    glBlitFramebuffer(0, 0, fb.width, fb.height, 0, 0, width, height,
                      GL_COLOR_BUFFER_BIT, GL_LINEAR);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    // finish
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
  }

  void resize(uint32_t width, uint32_t height) {
    this->width = width;
    this->height = height;
    ctx.resize(width, height);
  }

  bool should_exit() const { return should_exit_flag; }

  ~app_main() {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
    glDeleteBuffers(1, &image);
  }

private:
  GLFWwindow *window;
  uint32_t width, height;
  bool should_exit_flag{};
  cmrc::embedded_filesystem fs{cmrc::resources::get_filesystem()};
  std::unique_ptr<mesh> bunny{}, bunny2{}, teapot{}, floor{}, light_wall_z{};
  std::unique_ptr<facet_geometry> skybox{};
  std::unique_ptr<texture> envmap{};
  std::vector<mesh *> scene{};
  struct camera_t {
    float3 pos{3.0f, 2.1f, 3.0f};
    float3 dir{-0.644f, -0.412f, -0.644f};
    float3 up{0.0f, 1.0f, 0.0f};
    float fov{0.5f};

  private:
    friend bool operator!=(const camera_t &l, const camera_t &r) {
      return distance(l.pos, r.pos) > 1e-5 || distance(l.dir, r.dir) > 1e-5 ||
             distance(l.up, r.up) > 1e-5 || std::fabs(l.fov - r.fov) > 1e-5;
    };
  } camera;
  // rasterization context
  phong program{};
  envmap_shader envmap_program{};
  context ctx{width, height};
  // rt context
  rt_context rt_ctx{};
  std::vector<hit_record> hits;
  std::vector<instance> instances;
  bool invalidate_samples{};
  shader_binding_table sbt{};
  shader_binding_table clear_samples{};
  traversable_handle root_handle{};
  launch params{};
  camera_t prev_camera_params{};
  // intermediate buffer
  GLuint image{};
  GLuint readFboId{};
  int32_t mode = 0; // 0 - rasterization, 1 - raytracing
  bool rotate{};
  float rotated_radians{math::pi<float>() / 2.0f};

  void update_camera() {
    constexpr float camera_speed = 0.2f;
    const auto up = camera.up; // +y
    const auto dir = normalize(camera.dir);
    const auto right = normalize(cross(dir, up));   // -x
    const auto left = -right;                       // +x
    const auto front = normalize(cross(up, right)); // -x
    const auto back = -front;                       // +z
    float3x3 world_to_camera{left, up, back};
    float3x3 camera_to_world = inverse(world_to_camera);

    constexpr float camera_pitch_speed = math::radians(-1.0f);
    constexpr float camera_yaw_speed = math::radians(2.0f);
    constexpr float camera_min_pitch = math::radians(10.0f),
                    camera_max_pitch = math::radians(170.0f);
    float pitch = std::acos(dot(up, normalize(dir))); // [0, 180)
    const auto projection = mul(camera_to_world, dir - dot(dir, up) * up);
    float yaw = atan2(projection.z, projection.x);
    float3 d_pos;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) { // front
      d_pos = camera_speed * front;
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) { // left
      d_pos = camera_speed * left;
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) { // back
      d_pos = camera_speed * back;
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) { // right
      d_pos = camera_speed * right;
    }
    if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) { // up
      d_pos = camera_speed * up;
    }
    if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS) { // down
      d_pos = camera_speed * -up;
    }
    camera.pos += d_pos;

    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) { // pitch up
      pitch = std::max(camera_min_pitch, pitch + camera_pitch_speed);
    }
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) { // pitch down
      pitch = std::min(camera_max_pitch, pitch - camera_pitch_speed);
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) { // yaw left
      yaw += camera_yaw_speed;
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) { // yaw right
      yaw -= camera_yaw_speed;
    }
    float3 direction;
    direction.x = cos(yaw) * sin(pitch);
    direction.y = cos(pitch);
    direction.z = sin(yaw) * sin(pitch);
    direction = mul(world_to_camera, direction);
    camera.dir = direction;
  }

  void update_instance_as() {
    assert(instances.size() == hits.size());
    for (size_t i = 0; i != instances.size(); ++i) {
      auto *obj = static_cast<mesh *>(hits[i].payload);
      instances[i].transform = convert(obj->matrix());
    }
    instance_as_build_options options{
        instances.data(),
        static_cast<uint32_t>(instances.size()),
    };
    rt_ctx.update_instance_as(root_handle, &options);
  }

  std::istringstream open_resource(const char *path) {
    auto file_bunny = fs.open(path);
    return std::istringstream(std::string(file_bunny.begin(), file_bunny.end()),
                              std::ios_base::binary);
  }

  void init_scene() {
    auto bunny_geom = load_geometry(open_resource("res/bunny.obj"));
    // bunny
    bunny = std::make_unique<mesh>(
        bunny_geom, load_texture(open_resource("res/bunny_diffuse.jpg"),
                                 TextureMode::SRGB));
    bunny->set_quaternion(rotation_quat(float3{0.0f, 1.0f, 0.0f}, 1.5707f));
    bunny->set_position(float3(0.3f, 0.0f, 0.0f));
    bunny->set_scale(float3(3.0f));
    // transparent teapot
    teapot = std::make_unique<mesh>(
        load_geometry(open_resource("res/teapot.obj")),
        create_color_texture(float4(0.62f, 0.23f, 0.69f, 0.5f)));
    teapot->set_quaternion(rotation_quat(float3{0.0f, 1.0f, 0.0f}, 1.5707f));
    teapot->set_scale(float3(0.025f));
    teapot->set_position(float3(-0.4f, 0.2f, 0.0f));
    teapot->roughness = 0.01f;
    teapot->opacity = 0.0f;
    teapot->ior = 1.5f;
    // floor
    floor = std::make_unique<mesh>(
        load_geometry(open_resource("res/floor.obj")),
        load_texture(open_resource("res/floor_diffuse.tga")));
    floor->roughness = 0.05f;
    floor->metallic = 1.0f;
    // light source
    light_wall_z = std::make_unique<mesh>(
        load_geometry(open_resource("res/wall.obj")),
        create_color_texture(float4(0.6f, 0.6f, 0.1f, 1.0f)));
    light_wall_z->emission = float3(10.0f, 10.0f, 0.2f);
    scene = std::vector<mesh *>{
        bunny.get(),
        floor.get(),
        light_wall_z.get(),
        teapot.get(),
    };
    // skybox
    envmap = std::make_unique<texture>(
        load_texture(open_resource("res/sunset.hdr"), TextureMode::LINEAR));
    skybox = std::make_unique<facet_geometry>(load_geometry(open_resource("res/box.obj")));
  }

  void init_rt() {
    // build AS
    miss_record miss{
        ray_miss,
        nullptr,
    };
    for (auto *obj : scene) {
      geometry_as_build_options options{
          static_cast<uint32_t>(hits.size()),
          float3x4{{1.f, 0.f, 0.f},
                   {0.f, 1.f, 0.f},
                   {0.f, 0.f, 1.f},
                   {0.f, 0.f, 0.f}},
          &obj->geometry,
      };
      auto handle = rt_ctx.build_geometry_as(&options);
      instances.push_back({
          0,
          static_cast<uint32_t>(instances.size()),
          convert(obj->matrix()),
          handle,
      });
      hits.push_back(hit_record{ray_closest_hit, obj});
      // hits[0].closest_hit(nullptr, nullptr);
    }
    instance_as_build_options options{
        instances.data(),
        static_cast<uint32_t>(instances.size()),
    };
    root_handle = rt_ctx.build_instance_as(&options);
    // build SBT
    sbt = shader_binding_table{
        raygen,
        hits.data(),
        static_cast<uint32_t>(hits.size()),
        miss,
    };
    clear_samples = shader_binding_table{
        [](traverse_context *ctx, void *launch_params, uint2 index) {
          auto &params = *reinterpret_cast<launch *>(launch_params);
          uint32_t pixel_id = index.y * params.width + index.x;
          ray_data *prd = params.prd.data() + pixel_id;
          prd->color = float3{0.0f};
        },
        nullptr,
        0,
        {},
    };
  }
};

int windowLoop() {
  if (!glfwInit()) {
    return 1;
  }
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_SCALE_TO_MONITOR, 1);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  GLFWwindow *window = glfwCreateWindow(1280, 720, "Renderer", NULL, NULL);
  int width, height;
  if (window == nullptr) {
    return 1;
  }
  glfwMakeContextCurrent(window);
  glfwGetFramebufferSize(window, &width, &height);

  int version = gladLoadGL(glfwGetProcAddress);
  if (version == 0) {
    return 1;
  }

  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  {
    app_main app{window, static_cast<uint32_t>(width),
                 static_cast<uint32_t>(height)};
    app.init();
    do {
      app.update();
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      app.draw();
      glfwSwapBuffers(window);
      glfwPollEvents();
    } while (!app.should_exit() && glfwWindowShouldClose(window) == 0);
  }
  glfwTerminate();
  return 0;
}

int main(int argc, char *argv[]) { return windowLoop(); }

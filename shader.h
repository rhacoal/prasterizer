#ifndef PRASTERIZER_SHADER_H
#define PRASTERIZER_SHADER_H
#include "common.h"

#include <cstddef>

struct vertex_attributes {
  float3 position;
  float2 texcoord;
  float3 normal;
};

/// @brief Base class for shaders.
class shader {
public:
  virtual float4 vertex(const vertex_attributes &attrib) = 0;

  virtual bool fragment(float4 &color) = 0;

  static float4 sample(const texture &tex, const float2 &uv);

  float frag_depth() const;

  float2 &varying2f(uint32_t index);

  float3 &varying3f(uint32_t index);

  float4 &varying4f(uint32_t index);

private:
  friend class context;

  void use();

  void vary(const float3 &bar);

  void set_vertex_index(uint32_t vertex_idx);

  void set_frag_depth(float frag_depth);
};
#endif

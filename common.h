#ifndef PRASTERIZER_COMMON_H
#define PRASTERIZER_COMMON_H
#include <cstdint>
#include <linalg.h>

using namespace linalg::aliases;

class shader;
class context;

struct facet_geometry;
struct texture;

struct rgba {
  explicit rgba(uint32_t data) : data(data) {}
  rgba(uint8_t r, uint8_t g, uint8_t b, uint8_t a) : r(r), g(g), b(b), a(a) {}

  union {
    struct {
      uint8_t r, g, b, a;
    };
    uint32_t data;
  };
};
#endif
